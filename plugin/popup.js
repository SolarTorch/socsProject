// popup.js
// Run when browser action triggered (when user clicks our icon)
// Interacts with original page through content scripts and has direct
// access to the popup

function getCurrentTab(callback) {
  let queryInfo = {
    active: true,
    currentWindow: true
  };
  chrome.tabs.query(queryInfo, function(tabs) {
    callback(tabs[0]);
  });
}

function renderStatus(statusText) {
  document.getElementById('status').textContent = statusText;
}

// main function of browser action - identify existing annotations
document.addEventListener('DOMContentLoaded', function() {
  getCurrentTab(function(tab) {
    let url = tab.url
    renderStatus("Checking for annotations");
    // run content script to examine page and check firebase
    chrome.tabs.executeScript(null, {file: "firebase.js"}, function() {
      chrome.tabs.executeScript(null, {file: "hilitor.js"}, function() {
        chrome.tabs.executeScript(null, {file: "pageScraper.js"}, function() {
          chrome.tabs.sendMessage(tab.id, {"command": "checkAnnotations"}, function(response) {
            renderStatus(response);
          });
        });
      });
    });
  });
});
