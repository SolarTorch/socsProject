// This context menu script runs in the background
// To modify the original page, needs to send message
function onCLickHandler(info) {
  console.log("Context Menu Add Annotation clicked");
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
    chrome.tabs.sendMessage(tabs[0].id, {"command": "addAnnotation"});
  });
};

chrome.contextMenus.onClicked.addListener(onCLickHandler);

// set up context menu on install
chrome.runtime.onInstalled.addListener(function() {
  let context = "selection";
  let title = "Add Annotation";
  // only one menu, we don't really need the id
  let id = chrome.contextMenus.create({
      "title": title,
      "contexts": [context],
      "id": "context"+context
  })
});
