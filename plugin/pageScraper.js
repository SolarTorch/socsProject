// Setup, content script stays alive between browser_actions
// Only need to setup variables and listener once
if (typeof setup === 'undefined') {
  console.log("content page setup");
  setup = true;
  // responses
  var UNSUPPORTED_SITE = "RumourEval currently does not support this site";
  var SUCCESS = "Success";
  // patterns
  var patterns = [
    "reddit.com\/r\/politics\/*",
    "facebook.com\/*"
  ];
  var sites = {
    "reddit.com\/r\/politics\/*" : "reddit",
    "facebook.com\/*" : "facebook"
  };
  addMessageListener();
}
// Initialize Firebase, this needs to be done everytime for some reason
var config = {
  apiKey: "AIzaSyBvtQqdx0V8m4hZGoAhiuff81jHMcwcHo4",
  authDomain: "socs-9d990.firebaseapp.com",
  databaseURL: "https://socs-9d990.firebaseio.com",
  projectId: "socs-9d990",
  storageBucket: "socs-9d990.appspot.com",
  messagingSenderId: "654405679037"
};
firebase.initializeApp(config);

function addLinkToFirebase() {
  firebase.database().ref('reddit/permalink').set({
    support: 'hi',
    deny: 'world'
  });
}

// returns site or undefined
function verifySupportedSite(url) {
  let pattern = patterns.find(function(elt) {
    return RegExp(elt).test(url);
  });
  return sites[pattern];
}

// Listener parses page, queries firebase, and marksup any annotated text
function addMessageListener() {
  // needs to return true to follow some async rules
  chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    let site = verifySupportedSite(document.URL);
    if (typeof site === 'undefined') {
      sendResponse(UNSUPPORTED_SITE);
      return true;
    }

    switch(request.command) {
      case 'checkAnnotations':
        checkAnnotations(sendResponse);
        break;
      case 'addAnnotation':
        getAnnotationHTML();
        break;
    }
    return true;
  });
}

function checkAnnotations(sendResponse) {
  console.log("Checking page for annotations");
  let entryNodes = document.querySelector('div.content').querySelectorAll('div.entry');
  entryNodes.forEach(examineEntry);
  if (sendResponse != null) {
    sendResponse("Annotations are now marked");
  }
}

function examineEntry(node) {
  let permalink = getPermalink(node);
  if (typeof permalink === 'undefined' || permalink == null) {
    return;
  }
  // send query to firebase
  firebase.database().ref('reddit/' + permalink).once('value').then(function(snapshot) {
    if (snapshot.val() === null) {
      return;
    }
    // for each textSnippet in this entry, find and highlight
    snapshot.forEach(function(textSnapshot) {
      var textSnippet = decodeDBSafe(textSnapshot.key);
      let count = {"support":0, "neutral":0, "deny":0};
      let colors = {"support":2, "neutral":0, "deny":3}
      textSnapshot.forEach(function(annotationSnapshot){
        count[annotationSnapshot.val()["stance"]]++;
      });
      let maxStance = Object.keys(count).reduce(function(a, b){ return count[a] > count[b] ? a : b });
      console.log("hilitor search for: " + textSnippet + " with color " + colors[maxStance]);
      let snippetHilitor = new Hilitor(node);
      snippetHilitor.apply(textSnippet, colors[maxStance]);
    });
  });
}

function getAnnotationHTML(text, selection) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var div = document.createElement('div');
      div.innerHTML = this.responseText;
      div.className += "annotationForm";
      addAnnotationForm(div, text, selection);
    } else {
      //console.log('files not found');
    }
  };
  xhttp.open("GET", chrome.runtime.getURL("annotationForm.html"), true);
  xhttp.send();
}
// Insert into page
function addAnnotationForm(annotationForm, selectedText = window.getSelection().toString(), selection = window.getSelection().anchorNode.parentNode) {
  selectedText = encodeDBSafe(selectedText)
  let permalink = getPermalink(selection);
  console.log("Getting annotation form for " + permalink)

  // hookup functionality
  let supportLink = annotationForm.getElementsByClassName('socsSupport')[0];
  supportLink.addEventListener("click", function(e) {
    addLink(e, annotationForm, selectedText, permalink, "support");
  });
  let neutralLink = annotationForm.getElementsByClassName('socsNeutral')[0];
  neutralLink.addEventListener("click", function(e) {
    addLink(e, annotationForm, selectedText, permalink, "neutral");
  });
  let denyLink = annotationForm.getElementsByClassName('socsDeny')[0];
  denyLink.addEventListener("click", function(e) {
    addLink(e, annotationForm, selectedText, permalink, "deny");
  });
  let cancelButton = annotationForm.getElementsByClassName('socsCancel')[0];
  cancelButton.addEventListener("click", function(e) {
    removeAnnotationForm();
  });
  // Prevent enter on text box
  let textInput = annotationForm.getElementsByClassName('socsLinkInput')[0];
  textInput.addEventListener("keydown", function(e) {
    if (e.keyCode == 13) {
      e.preventDefault();
    }
  });

  // Add existing article links
  let fbpath = 'reddit/' + permalink + '/' + selectedText;
  console.log("Reading from firebase: " + fbpath);
  firebase.database().ref(fbpath).once('value').then(function(snapshot) {
    if (snapshot.val() === null) {
      return;
    }
    // Add articles to the annotationform
    snapshot.forEach(function(childSnapshot) {
      var annotation = decodeDBSafe(childSnapshot.key);
      var annotationNode = document.createElement("LI")
      annotationNode.className = "socsLink";
      let urlArray = annotation.split("/");
      let linkText = urlArray.pop();
      // url ended with slash
      if (linkText.length == 0) {
        linkText = urlArray.pop();
      }
      var textNode = document.createTextNode(linkText);
      var linkNode = document.createElement('a');
      linkNode.appendChild(textNode);
      linkNode.title = "annotation link";
      linkNode.href = annotation;
      annotationNode.appendChild(linkNode);
      var parent = null;
      var attrs = childSnapshot.val();
      console.log("Adding link " + annotation + " with stance: " + attrs["stance"]);
      switch (attrs["stance"]) {
        case "support":
          parent = annotationForm.getElementsByClassName('socsSupportLinks')[0];
          break;
        case "neutral":
          parent = annotationForm.getElementsByClassName('socsNeutralLinks')[0];
          break;
        case "deny":
          parent = annotationForm.getElementsByClassName('socsDenyLinks')[0];
          break;
      }
      parent.appendChild(annotationNode);
    });
  });
  // Add to page
  let parentElt = selection.closest('.hilitorAnnotationWrapper');
  if (parentElt == null) {
    parentElt = selection;
  }
  parentElt.insertBefore(annotationForm, parentElt.childNodes[0]);
}
// Add to firebase
function addLink(e, annotationForm, selectedText, permalink, type) {
  e.preventDefault();
  let annotation = annotationForm.getElementsByClassName('socsLinkInput')[0].value;
  console.log("adding " + type + " annotation [" + annotation + "] for '" + selectedText + "' with perma " + permalink);
  let path = 'reddit/' + permalink + '/' + selectedText + '/' + encodeDBSafe(annotation);
  console.log("writing to " + path)
  firebase.database().ref(path).set({
    stance: type,
    veracity: 1.0,
    author: "test"
  });
  removeAnnotationForm();
  checkAnnotations();
}

function getPermalink(node) {
  let parentEntry = node.closest('.entry');
  if (parentEntry == null) {
    return null;
  }
  let permalinkAnchor = parentEntry.querySelector('.first a');
  if (permalinkAnchor == null) {
    return null;
  }
  let paths = permalinkAnchor.href.split('/')
  return paths[4] + '/' + paths[6] + '/' + paths[8]
}

function encodeDBSafe(s) {
  return encodeURIComponent(s).replace(/\./g, '%2E');
}
function decodeDBSafe(s) {
  return decodeURIComponent(s).replace(/%2E/g, '\.');
}

function removeAnnotationForm() {
  document.getElementsByClassName('annotationForm')[0].remove();
}
