import string
import numpy as np
import pandas as pd
import nltk #Natural Language Toolkit
import matplotlib
import matplotlib.pyplot as plt

data = pd.read_csv("../fake.csv")
fake_news = data[data['type'] == "fake"]

fake_news.title.fillna("", inplace=True)
fake_news.text.fillna("", inplace=True)

fake_text = fake_news.title.str.cat(fake_news.text, sep = ' ')
fake_words = nltk.word_tokenize(' '.join(fake_text.tolist()))
