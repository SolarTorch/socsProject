var natural = require('natural');
// var fs = require('fs');
// var stemmer = natural.PorterStemmer;
var classifier = new natural.BayesClassifier();

var train = [];
var test = [];

var para = "quick fox jump over a leazy dog.";
// var paras = fs.readFileSync('filename.txt', 'utf-8');
var keyword = "keyword";

// token that split a paragraph into sentences
var tokenizer = new natural.RegexpTokenizer({pattern: /[!?.]/})
// token that split a sentence into words
var token = new natural.WordTokenizer();
// var spellcheck = new natural.Spellcheck(token);

var words = token.tokenize(para);

train.forEach(function(item) {
    // label states if the text is fake
    classifier.addDocument(item.text, item.label);
});

classifier.train();

test.forEach(function(item) {
    var lab = classifier.classify(item.text);
});

console.log(words);

for (i = 0; i < words.length; ++i) {
    // words[i] = stemmer.stem(words[i]);
    // console.log(spellcheck.getCorrections(words[i]));
    console.log(words[i]);
}